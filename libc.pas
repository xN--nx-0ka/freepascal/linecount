unit libc;

{$linklib c}

interface

uses 
  ctypes;

type size_t = cuint64;

function strerror(ErrorNum: Integer): PChar; cdecl; external;
function memchr(const s: Pointer; c: cint; n: size_t): Pointer; cdecl; external;

implementation

end.
