program LineCount;

{$mode ObjFPC}

uses 
  BaseUnix, Unix,
  StrUtils,
  libc;

type
  {$ifdef cpu64}
    ptruint = QWord;
    ptrint  = Int64;
  {$else}
    ptruint = LongWord;
    ptrint  = LongInt;
  {$endif}


procedure usage();
var 
  prog: String;
  pos : Integer;
begin
  prog := ParamStr(0);
  pos := RPos('/', prog);
  Delete(prog, 1, pos);
  
  WriteLn('usage: ', prog, ' FILENAME');
  WriteLn();
  WriteLn('Print number of lines for a text file.');
end;

var
  fname : string;
  fd    : CInt;
  finfo : Stat;
  fsize : LongInt;
  count : Integer;
  pmem  : PChar;
  ptr   : PChar;
  size  : CInt;
  errsv : CInt;
begin
  if (ParamCount() <> 1) then
  begin
    usage();
    halt(1);
  end;
  fname := ParamStr(1);

  // Datei öffnen
  fd := fpOpen(fname, O_RdOnly);
  if (fd = -1) then
  begin
    errsv := FpGetErrno;
    WriteLn('Open ''', fname, ''' failed. Errno : ', errsv, ' ', strerror(errsv));
    halt(errsv);
  end;

  // Dateigrösse ermitteln
  if (FpFStat(fd, finfo) <> 0) then
  begin
    errsv := FpGetErrno;
    WriteLn('Fstat failed. Errno : ',errsv , ' ', strerror(errsv));
    fpclose(fd);
    halt (1);
  end;
  fsize := finfo.st_size;

  // Memory Mapping
  pmem := PChar(FpMMap(nil, fsize, PROT_READ or PROT_WRITE ,MAP_PRIVATE, fd, 0));
  if (ptruint(pmem) = ptruint(-1)) then
  begin
    errsv := FpGetErrno;
    WriteLn('mmap failed. Errno : ',errsv , ' ', strerror(errsv));
    fpclose(fd);
    halt (errsv);
  end;
  
  fpclose(fd);

  // '\n' zählen
  count := 0;
  ptr := memchr(pmem, 10, fsize);
  while (ptr <> nil) do
  begin
    Inc(count);
    Inc(ptr);
    ptr := memchr(ptr+1, 10, fsize -(ptr - pmem));
  end;
  WriteLn(count, ' ', fname);

  // Memory freigeben und beenden
  if (FpMunMap(pmem, finfo.st_size) <> 0) then
  begin
    errsv := FpGetErrno;
    WriteLn('munmap failed. Errno : ',errsv , ' ', strerror(errsv));
    Halt(fpgeterrno);
  end;
end.
