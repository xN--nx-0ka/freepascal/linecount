TARGET = lc

UNITS = libc

LIBS = c


PPUS = $(addsuffix .ppu, $(UNITS))

PC = fpc

CFLAGS := -O2 -CX
LFLAGS := -O2 -Xs -XX $(addprefix -k-l, $(LIBS))


%.ppu: %.pas
	$(PC) $(CFLAGS) $<

all: $(TARGET)

$(TARGET): $(TARGET).pas $(PPUS)
	$(PC) -o$@ $(LFLAGS) $@.pas

clean:
	rm -f $(TARGET) $(TARGET).o $(PPUS) 
